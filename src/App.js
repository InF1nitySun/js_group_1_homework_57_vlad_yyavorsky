import React, {Component} from 'react';
import './App.css';
import AddForm from "./Components/AddForm/AddForm";
import MainField from "./Components/MainField/MainField";



    let itemId = 0;

class App extends Component {


    state = {
        items: [],
        total: 0,
        name: '',
        cost: '',
    };


    changeName = (e) => {
        let name = e.target.value;
        this.setState({name});
    };
    changeValue = (e) => {
        let cost = e.target.value;
        this.setState({cost});
    };

    addItem = () => {
        let items = [...this.state.items];
        let name = this.state.name;
        let cost = this.state.cost;

        if(name && cost) {

            if(!name.match(/[A-z 0-9]+/i) || isNaN(parseInt(cost, 10))) {
                alert('проверьте введенные данные');
            }else {
                items.push({name: name, cost: cost, id: itemId});
                itemId++;

                this.setState({items});
            }
        }
    };

    remItem = (itemId) => {
        let items = [...this.state.items];
        const index = this.state.items.findIndex(item => {

            return item.id === itemId
        });

        items.splice(index, 1);

        this.setState({items});
    };

    render() {
        return (
            <div className="App">
                <AddForm changeName={this.changeName} changeValue={this.changeValue} click={this.addItem}/>
                <MainField items={this.state.items} sum={this.state.total} click={this.remItem}/>
            </div>
        );
    }
}

export default App;

