import React from 'react';
import './AddForm.css';

const AddForm = props => {
    return (

        <div className='AddForm'>
            <input onChange={props.changeName} type="text" placeholder='Item Name'/>
            <input onChange={props.changeValue} type="text" placeholder='Cost'/>
            <span> KGS</span>
            <button onClick={props.click}>Add</button>

        </div>

    )
};

export default AddForm;