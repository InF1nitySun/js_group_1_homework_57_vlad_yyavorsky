import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div style={{marginTop: '10px'}} className='Item'>
            <span className='ItemName'> {props.value} </span>
            <span className='Price'> {props.price} KGS</span>
            <button className='Button' onClick={props.click}>X</button>
        </div>
    )
};

export default Item;