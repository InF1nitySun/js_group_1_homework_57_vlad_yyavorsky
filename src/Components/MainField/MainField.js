import React from 'react';
import Item from "./Item/Item";
import TotalSpent from "./TotalSpent/TotalSpent";
import './MainField.css';


const MainField = props => {
    let totalPrice = 0;
    return (
        <div className='MainField'>
            {props.items.map(item => {
                totalPrice += parseInt(item.cost, 10);
                return <Item value={item.name} price={item.cost} click={() => props.click(item.id)} key={item.id}/>
            } )}

            <TotalSpent sum={totalPrice}/>
        </div>
    )
};

export default MainField;