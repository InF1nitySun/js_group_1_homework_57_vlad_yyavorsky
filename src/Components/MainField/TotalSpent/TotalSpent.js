import React from 'react';
import './TotalSpent.css';

const TotalSpent = props => {
    return (
        <div>
            <span className='TotalSpent'>Total spent {props.sum} KGS</span>
        </div>
    )
};

export default TotalSpent;