const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

//1, 2 задание

let frontEnd = () => {
    return tasks.filter(item => item.category ==='Frontend')
        .reduce((sum, current) => sum + current.timeSpent, 0);
    };

let bugTime = () => {
    return tasks.filter(item => item.type === 'bug')
        .reduce((sum, current) => sum + current.timeSpent, 0);
};

console.log (frontEnd, bugTime);

//3

let titleUI = tasks.reduce(function(acc, tasks) {
    let re = 'UI';
    let str = tasks.title;

    if(str.includes(re)){
        acc++;
    }
    return acc;
}, 0);

console.log(titleUI);

//4
const rankFreq = tasks.reduce((acc, el) => {
    if (!acc[el.category]) {
        acc[el.category] = 1;
    } else {
        acc[el.category]++;
    }
    return acc;
}, {});


console.log(rankFreq);

//5
const More4Hours = tasks.filter((task) => {
    return  task.timeSpent > 4;
}).map(function(item){
    return {title: item.title,
        category:item.category}
});

console.log(More4Hours);